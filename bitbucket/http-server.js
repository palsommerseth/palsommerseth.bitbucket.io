var express = require("express");
var app = express();
var cookieParser = require("cookie-parser");
app.use(cookieParser());
app.use(function(request, response) {
  if (request.url === "/login") {
    response.cookie("logged-in", "true");
    response.send("De er innlogget, herr Sommerseth!");
  } else if (request.cookies["logged-in"] === "true") {
    response.send("OK");
  } else {
    response.status(401);
    response.send("Ikke OK");
  }
});
app.listen(3000);
