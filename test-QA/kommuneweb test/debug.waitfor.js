function waitFor({ text = "", selector = "*", click = false, focus = false }) {
    let matchingElements = Array.from(document.querySelectorAll(selector))
      .filter(element => element.textContent.includes(text))
      .sort(shortestTextVisibleIfSameLength) // shortest text first, e.g. "best" search result
  
    if (matchingElements.length > 1) {
      console.warn(`got ${matchingElements.length} matches for { text: ${text}, selector: ${selector} }`)
      console.warn(matchingElements)
    }
  
    if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
        if (click) {
            matchingElements[0].click()
        }
        if (focus) {
          matchingElements[0].focus()
        }
      return matchingElements[0];
    }
  
    let shadowedElements = Array.from(document.querySelectorAll(selector))
      .filter(element => element.shadowRoot)
      .flatMap(element =>
        Array.from(element.shadowRoot.querySelectorAll(selector))
      )
      .filter(element => element.textContent.includes(text))
      .sort(shortestTextVisibleIfSameLength)
  
    if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
      if (click) {
          shadowedElements[0].click()
      }
    return shadowedElements[0];
    }
  
    return null;
  
    function shortestTextVisibleIfSameLength(a, b) {
      let difference = a.textContent.length - b.textContent.length;
      if (difference === 0) {
          let aVisible = isVisible(a)
          let bVisible = isVisible(b)
  
          if (aVisible && !bVisible) {
              return -1
          } else if (!aVisible && bVisible) {
              return 1
          } else {
              return 0
          }
      }
      return difference
    }
  
    function isVisible(element) {
        return element.offsetParent !== null
    }
  }