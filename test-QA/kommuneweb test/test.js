const configuration = require("./package.json");
const puppeteer = configuration.devDependencies.puppeteer
  ? require("puppeteer")
  : require("puppeteer-firefox");

// configuration
const mainPage = `https://www.trondheim.kommune.no/`;
const headless = false; // false: show browser, true: hide browser
const assert = require("assert");
const slowMo = true // true: each browser action will take 100 milliseconds
  ? 100
  : 0;

// globals
let browser = null;
let page = null;

before(async function() {
  this.timeout(10 * 1000); // starting browser may take more than 2 seconds

  browser = await puppeteer.launch({ headless, slowMo });
  page = (await browser.pages())[0];

  // page.on('console', async function (msg) {
  //     if (msg.type() === 'error' && msg.args().length) {
  //         let args = await Promise.all(msg.args().map(arg => arg.jsonValue()))
  //         console.error("Browser console.error:", ...args)
  //     } else {
  //         console.log(msg._text)
  //     }
  // })
});

after(function() {
  // browser.close()
});

describe("kommuneweb", function() {
  this.timeout(slowMo === 0 ? 5000 : 0);

  before(async function() {
    this.timeout(5 * 1000);
    await page.goto(mainPage);
    page.setViewport({
      width: 1280,
      height: 1280
    });
  });

  it("Søk etter barnehage", async function() {
    await waitFor({
      selector: 'input[placeholder="Hva kan vi hjelpe deg med?"]',
      focus: true
    });
    await page.keyboard.type("barnehage");
    await waitFor({ text: "Søk", click: true });
    await waitFor({ text: "Søk om plass, endre oppholdstid" });
  });
  it("barnehageliste", async function() {
    await page.goto("https://www.trondheim.kommune.no/barnehageplass/");
    await waitFor({
      text: "Hvilke barnehager kan jeg velge",
      selector: "a",
      click: true
    });
    await waitFor({
      text: "Oversikt over barnehager i Trondheim.",
      selector: "a",
      click: true
    });
    await waitFor({ text: "Angelltrøa" });
    await waitFor({
      selector: 'input[placeholder="Din adresse"]',
      focus: true
    });
    await page.keyboard.type("Munkegata 1");
    await page.keyboard.press("Enter");
    await waitFor({ text: "348m" });
  });

  it("Naviger i menyene", async function() {
    await page.goto(mainPage);
    await waitFor({ text: "Veg, vann og avløp", click: true });
    await waitFor({ text: "Meld inn feil på vann og avløp", click: true });
    await waitFor({ text: "72 54 63 50" });
  });

  it("søk ansattlister", async function() {
    await page.goto("https://www.trondheim.kommune.no/sokeside/");
    await page.keyboard.type("Trondheim eiendom");
    await page.keyboard.press("Enter");
    await waitFor({ text: "Enheter", click: true });
    await waitFor({ text: "Trondheim eiendom", selector: "a", click: true });
    await waitFor({ text: "Finn ansatt og se kart", click: true });
    await waitFor({ text: "Støen", selector: "p" }); // selector p fordi synlighetstesten ikke virker på innhold fra bluegarden
    await waitFor({ text: "Se alle ansatte", selector: "a", click: true });
    await waitFor({ text: "Renholdsoperatør" });
    await waitFor({
      text: "Trondheim eiendom",
      selector: ".leaflet-popup-content"
    });
  });

  it("kommunikasjonsenheten", async function() {
    await page.goto(
      "https://www.trondheim.kommune.no/org/organisasjon/kommunikasjonsenheten/"
    );
    await waitFor({ text: "Finn ansatt og se kart", click: true });
    await waitFor({
      selector: 'input[placeholder="Søk etter ansatt"]',
      focus: true
    });
    await page.keyboard.type("Rolf");
  });

  it("Engelsk site", async function() {
    await page.goto("https://www.trondheim.kommune.no/english/");
    await waitFor({ text: "Opening hours", click: true });
    await waitFor({ text: "by self-service" });
  });

  it("Underenheter", async function() {
    await page.goto(
      "https://www.trondheim.kommune.no/org/oppvekst/barnehager/sildrapen-bhgr/"
    );
    await waitFor({ text: "Åsvang barnehage", selector: "a", click: true });
    await waitFor({ text: "Bergslia" });
    await waitFor({ text: "Lohove barnehage", selector: "a", click: true });
    await waitFor({ text: "Kleiva" });
    await waitFor({ text: "Angelltrøa barnehage", selector: "a", click: true });
    await waitFor({ text: "Prikken" });
    await waitFor({ text: "Sildråpen barnehager", selector: "a", click: true });
    await waitFor({ text: "Sommerstengt" });
  });

  it("skjema", async function() {
    await page.goto(
      "https://www.trondheim.kommune.no/aktuelt/utvalgt/om-kommunen/skjema/skjema-fra-a-til-aa/"
    );
    await waitFor({ text: "Vis hele listen", click: true });
    await waitFor({ text: "Festivaler", click: true });
    await waitFor({ text: "Søknadsfrist" });
    await waitFor({ text: "Skjema fra A til Å", selector: "a", click: true });
    await waitFor({
      selector: 'input[placeholder="Skriv for å filtrere"]',
      focus: true
    });
    await page.keyboard.type("Elev");
    await waitFor({ text: "elevplass", click: true });
  });

  it("bestbet", async function() {
    await page.goto("https://www.trondheim.kommune.no/");
    await waitFor({
      selector: 'input[placeholder="Hva kan vi hjelpe deg med?"]',
      focus: true
    });
    await page.keyboard.type("søppel");
    await page.keyboard.press("Enter");
    await waitFor({ text: "Trondheim Renholdsverk", click: true });
    await waitFor({ text: "Finn din tømmeplan!" });
  });

it("politiker", async function() {
    await page.goto("https://www.trondheim.kommune.no/tema/politikk-og-planer/Politikk/parti-og-politikere/utvalg/");
    await waitFor({
      selector: 'input[placeholder="Søk i utvalgnavn  ..."]',
      focus: true
    }); 
    await page.keyboard.type("Bygnings");
    await waitFor({ text: "Bygningsrådet", click: true });


})

  it("episerver", async function() {
    await page.goto("https://www.trondheim.kommune.no/episerver/cms/");

    await waitFor({ text: "Alternativer", timeout: 10000 });
  });
});

// Biblioteket

describe("biblioteket", function() {
  this.timeout(slowMo === 0 ? 5000 : 0);

  before(async function() {
    this.timeout(5 * 1000);
    await page.goto("https://biblioteket.trondheim.kommune.no");
    page.setViewport({
      width: 1280,
      height: 1280
    });
  });

  it("førsteside", async function() {
    await waitFor({ text: "Aktuelle arrangement" });
  });
  it("Åpningstider", async function() {
    await waitFor({ text: "Sentrum", selector: "a", click: true });
    await waitFor({ text: "Mandag" });
    await waitFor({ text: "Lukk", click: true });
  });
  it("navigering", async function() {
    await waitFor({ text: "Meny", selector: "a", click: true });
    await waitFor({ text: "Tema", click: true });
  });

  it("bibliofil", async function() {
    await waitFor({
      selector: 'input[placeholder="Søk etter bok, film, musikk eller annet"]',
      focus: true
    });
    await page.keyboard.type("Ragde");
    await page.keyboard.press("Enter");
    await waitFor({ text: "Forfatter: Ragde" });
    await waitFor({ text: "Dvd", selector: "a", click: true });
    await waitFor({ text: "Berlinerpoplene" });
    await waitFor({
      selector: 'input[placeholder="Søk etter bok, film, musikk eller annet"]',
      focus: true
    });
    await waitFor({ text: "Meråpent bibliotek", selector: "a", click: true });
    await waitFor({ text: "kontrakt" });
  });

  it("kalender", async function() {
    // selector er til første arrangement på hovedsiden, kan endre seg
    await waitFor({
      selector: '#container > .list-by-date a[href="skip"]',
      click: true
    });
    await page.waitForNavigation();
    const url = await page.url();
    assert(
      url.includes("kalender"),
      "URL inneholder ikke ordet kalender etter klikk på første arrangement."
    );
  });

  it("megameny", async function() {
    await waitFor({ text: "Meny", selector: "a", click: true });
    await waitFor({ text: "Lånekort og regler", selector: "a", click: true});
    await waitFor({ text: "Det er gratis og enkelt å få lånekort"})
  });

  it("sidesøk", async function() {
    await page.goto("https://biblioteket.trondheim.kommune.no/innhold/om-biblioteket/tilbud/");
    await waitFor({ text: "Søk", selector: "a", click: true});
    await page.keyboard.type("Meråpent bibliotek");
    await waitFor({ text: "Biblioteket", selector: "a", click: true}); 
    await waitFor({ text: "Fakta om biblioteket", selector: "a", click: true});
    await waitFor({ text: "Velkommen til Trondheims største kulturhus"});

  });
  it("anbefaling", async function() {
    await page.goto("https://biblioteket.trondheim.kommune.no/innhold/om-biblioteket/tilbud/");
    await waitFor({ text: "Søk", selector: "a", click: true});
    await page.keyboard.type("Meråpent bibliotek");
    await waitFor({ text: "Se hele anbefalingen", selector: "a", click: true});
    await waitFor({ text: "Finn Boken", selector: "a", click: true});
    await waitFor({ text: "Glødestjernen"})
  }); 

  it("rss", async function() {
    await page.goto("https://biblioteket.trondheim.kommune.no/innhold/barn-og-unge/tips-og-anbefalinger/");
    await waitFor({ text: "Nye filmer for barn"});
  })


});

// intranett

describe("intranett", function() {
  this.timeout(slowMo === 0 ? 5000 : 0);

  before(async function() {
    page.on('dialog', async dialog => {
      await dialog.accept();
  }); // Godtar (lukker) alle dialogbokser
    this.timeout(5 * 1000);
    await page.goto("https://intranett.trondheim.kommune.no");
    page.setViewport({
      width: 1280,
      height: 1280
    });
  });

  it("intranettside", async function() {
    await waitFor({ text: "Nyheter", selector: "h2" });
  });

  it("dropdown", async function() {
    await waitFor({
      text: "Bygninger og transport",
      selector: "a",
      click: true
    });
    await waitFor({ text: "Møterom og lokaler", selector: "a", click: true });
  });

  it("minside", async function() {
    await waitFor({ text: "Snarveier", selector: "a", click: true });
    await waitFor({ text: "Min profil", selector: "a", click: true });
    await waitFor({ text: "Alle enheter" });
    await waitFor({ text: "Munkegata 1" });
  });

  it("menyvalg", async function() {
    await waitFor({ text: "Snarveier", selector: "a", click: true });
    await waitFor({ text: "Min enhet", selsctor: "a", click: true });
    await waitFor({ text: "Kommunikasjonsenheten" });
    await waitFor({ text: "Vis i kart", click: true });
    await waitFor({ text: "Vis mer", click: true });
    await waitFor({ text: "Ansatte", click: true });
    await waitFor({ text: "Kommunikasjonsrådgiver" });
    await waitFor({
      text: "Kommunikasjonsenheten",
      selector: "a",
      click: true
    });
    await waitFor({ text: "Endre side", selector: "a", click: true });
    await waitFor({ text: "Rediger innhold", selector: "span", click: true });
    await waitFor({ text: "Metadata" });
    await waitFor({ text: "Logg ut", click: true });
    await waitFor({ text: "IT-brukerhjelp", selector: "h2" });
  });

});

/**
 * Waits for a visible element containing given text, possibly clicks it.
 *
 * @param {object} params - What to wait for, in which selector, if it should be clicked and how long to wait.
 * @param {string} params.text - What text to wait for.
 * @param {string} params.selector - What selector to use, defaults to any element: `*`.
 * @param {bool} params.click - Wheter to click when found.
 * @param {number} params.timeout - How long to wait in milliseconds.
 */
async function waitFor({
  text = "",
  selector = "*",
  click = false,
  focus = false,
  timeout = 5000
}) {
  const start = Date.now();

  while (Date.now() - start < timeout) {
    let frames = await page.frames();
    let scopes = [page, ...frames];
    for (let scope of scopes) {
      let result;
      try {
        result = await scope.evaluate(pageFunction, text, selector, click);
      } catch (err) {
        // probably lost execution context, break and get new scopes
        break;
      }
      if (result) {
        return true;
      }
    }
  }

  throw new Error(
    `'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`
  );

  function pageFunction(text, selector, click) {
    let match = findElement(text, selector);

    if (match) {
      if (click) {
        match.click();
      }
      if (focus) {
        match.focus();
      }
      return true;
    }
    return false;

    function findElement(text, selector) {
      let matchingElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength); // shortest text first, e.g. "best" search result

      if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
        return matchingElements[0];
      }

      let shadowedElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.shadowRoot)
        .flatMap(element =>
          Array.from(element.shadowRoot.querySelectorAll(selector))
        )
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength);

      if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
        return shadowedElements[0];
      }

      return null;
    }

    function shortestTextVisibleIfSameLength(a, b) {
      let difference = a.textContent.length - b.textContent.length;
      if (difference === 0) {
        let aVisible = isVisible(a);
        let bVisible = isVisible(b);

        if (aVisible && !bVisible) {
          return -1;
        } else if (!aVisible && bVisible) {
          return 1;
        } else {
          return 0;
        }
      }
      return difference;
    }

    function isVisible(element) {
      return element.offsetParent !== null;
    }
  }
}
