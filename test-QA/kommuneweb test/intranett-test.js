const configuration = require("./package.json");
const puppeteer = configuration.devDependencies.puppeteer
  ? require("puppeteer")
  : require("puppeteer-firefox");

// configuration
const mainPage = `https://intranett.trondheim.kommune.no/`;
const headless = false; // false: show browser, true: hide browser
const assert = require("assert");
const slowMo = true // true: each browser action will take 100 milliseconds
  ? 100
  : 0;

// globals
let browser = null;
let page = null;

before(async function() {
  this.timeout(10 * 1000); // starting browser may take more than 2 seconds

  browser = await puppeteer.launch({ headless, slowMo });
  page = (await browser.pages())[0];

  // page.on('console', async function (msg) {
  //     if (msg.type() === 'error' && msg.args().length) {
  //         let args = await Promise.all(msg.args().map(arg => arg.jsonValue()))
  //         console.error("Browser console.error:", ...args)
  //     } else {
  //         console.log(msg._text)
  //     }
  // })
});

after(function() {
  // browser.close()
});

describe("interna", function() {
  this.timeout(slowMo === 0 ? 5000 : 0);

  before(async function() {
    this.timeout(5 * 1000);
    await page.goto(mainPage);
    page.setViewport({
      width: 1280,
      height: 1280
    });
  });

  it("førsteside", async function() {
      await waitFor({ text: "Nyheter", selector: "h2"});
  }); 
   
});




/**
 * Waits for a visible element containing given text, possibly clicks it.
 *
 * @param {object} params - What to wait for, in which selector, if it should be clicked and how long to wait.
 * @param {string} params.text - What text to wait for.
 * @param {string} params.selector - What selector to use, defaults to any element: `*`.
 * @param {bool} params.click - Wheter to click when found.
 * @param {number} params.timeout - How long to wait in milliseconds.
 */
async function waitFor({
  text = "",
  selector = "*",
  click = false,
  focus = false,
  timeout = 5000
}) {
  const start = Date.now();

  while (Date.now() - start < timeout) {
    let frames = await page.frames();
    let scopes = [page, ...frames];
    for (let scope of scopes) {
      let result;
      try {
        result = await scope.evaluate(pageFunction, text, selector, click);
      } catch (err) {
        // probably lost execution context, break and get new scopes
        break;
      }
      if (result) {
        return true;
      }
    }
  }

  throw new Error(
    `'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`
  );

  function pageFunction(text, selector, click) {
    let match = findElement(text, selector);

    if (match) {
      if (click) {
        match.click();
      }
      if (focus) {
        match.focus();
      }
      return true;
    }
    return false;

    function findElement(text, selector) {
      let matchingElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength); // shortest text first, e.g. "best" search result

      if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
        return matchingElements[0];
      }

      let shadowedElements = Array.from(document.querySelectorAll(selector))
        .filter(element => element.shadowRoot)
        .flatMap(element =>
          Array.from(element.shadowRoot.querySelectorAll(selector))
        )
        .filter(element => element.textContent.includes(text))
        .sort(shortestTextVisibleIfSameLength);

      if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
        return shadowedElements[0];
      }

      return null;
    }

    function shortestTextVisibleIfSameLength(a, b) {
      let difference = a.textContent.length - b.textContent.length;
      if (difference === 0) {
        let aVisible = isVisible(a);
        let bVisible = isVisible(b);

        if (aVisible && !bVisible) {
          return -1;
        } else if (!aVisible && bVisible) {
          return 1;
        } else {
          return 0;
        }
      }
      return difference;
    }

    function isVisible(element) {
      return element.offsetParent !== null;
    }
  }
}
