var state = {
    activity_types: [],
    max: 20,
    organisations: [],
    abortController: null,
};


fetch(`https://organisasjoner.trondheim.kommune.no/api/organisations/v1/flod_activity_types/`)
    .then(r => r.json())
    .then(activity_types => state.activity_types = activity_types);

$('#searchOrg').keyup(handleInput);

async function handleInput(event) {
    state.max = 20;
    state.organisations = [];
    renderState(state);

    if (state.abortController) {
        state.abortController.abort();
    }
    state.abortController = new AbortController();

    var searchText = event.target.value;

    if (event.target.value.length < 2) {
        return;
    }

    /**
     * * 0. split i flere ord
     * Navnesøk
     * 1. søk etter første ord
     * 2.ta bort de treffene som ikke har resten av ordene
     *       .then(orgs => addToState(orgs))
     * Aktivitetssøk
     * 1. ?
     * 
     */

    var words = searchText.toLowerCase().split(' ');

    var orgs = await Promise.all([
        searchByName(words),
        searchByActivity(words)]);

    addUniqueToState(flatten(orgs));
    renderState(state);

    // words.forEach((word) => {
    //  searchByText(word);
    // });
}
async function searchByName(searchTerms) {
    var firstSearchTerm = searchTerms[0];
    var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?name=${firstSearchTerm}`;
    var orgs = await fetchOrgs(url);
    var restOfSearchTerms = searchTerms.slice(1);
    var orgsMatchAllSearchTerms = orgs.filter(org =>
        restOfSearchTerms.every(searchTerm => org.name.toLowerCase().includes(searchTerm)));
    return orgsMatchAllSearchTerms;
}

async function searchByActivity(searchTerms) {

    var activitySearches = flatten(searchTerms.map(term => {
        // finner alle aktiviteter som passser med søkeordet
        var activities = state.activity_types.filter(activity => activity.name.toLowerCase().includes(term));
        // transformeres til et objekt med id og resten av søkeordene
        return activities.map(activity => {
            return {
                id: activity.id,
                restOfSearchTerms: searchTerms.filter(t => t !== term)
            }
        })
    }))

    // Konverter søkeord til liste med flere søkeresultater (promiseliste)
    var searchResults = activitySearches.map(activitySearch => {

        var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?flod_activity_type=${activitySearch.id}`;
        // henter alle organisasjoner på denne aktiviteten
        // ta bort organisasjoner som ikke passer på navn med de andre søkeordene

        var orgsMatchAllSearchTerms = fetchOrgs(url)
            .then(orgs =>
                orgs.filter(org => activitySearch.restOfSearchTerms.every(searchTerm => org.name.toLowerCase().includes(searchTerm))));
        return orgsMatchAllSearchTerms;
    });

    var listOfResults = await Promise.all(searchResults);
    return flatten(listOfResults);
}

function fetchOrgs(url) {
    return fetch(url, {
        signal: state.abortController.signal
    })
        .then(response => response.json())
        .then(orgs => orgs.filter(org => org.is_public !== false))
        .then(orgs => orgs.filter(isArtOrSport))
        .then(orgs => orgs.filter(isFrivilligLag))
        .catch(err => {
            console.error(err)
            return []
        })
}

function isArtOrSport(org) {
    return (

        org.brreg_activity_code.includes("1 200") ||
        org.brreg_activity_code.includes("1 100")
    )
}

function isFrivilligLag(org) {
    return org.org_form === "FLI"
}
function addUniqueToState(orgs) {
    orgs.sort(sortByName);

    var merged = [];
    orgs.forEach(org => {
        if (!merged.some(addedOrg => addedOrg.id === org.id)) {
            merged.push(org);
        }
    })

    state.organisations = merged;
}

function sortByName(a, b) {
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
        return 1;
    }
    else if (b.name.toLowerCase() > a.name.toLowerCase()) {
        return -1;
    }
    return 0;
}
function flatten(list, depth) {
    depth = (typeof depth == 'number') ? depth : Infinity;

    if (!depth) {
        if (Array.isArray(list)) {
            return list.map(function (i) { return i; });
        }
        return list;
    }

    return _flatten(list, 1);

    function _flatten(list, d) {
        return list.reduce(function (acc, item) {
            if (Array.isArray(item) && d < depth) {
                return acc.concat(_flatten(item, d + 1));
            }
            else {
                return acc.concat(item);
            }
        }, []);
    }
};

function renderState(state) {
    renderSearchResults(state.organisations)
}

function renderSearchResults(organisations) {
    $(`#searchResult`).empty();

    organisations.forEach((org, i) => {
        $(`
<div class="card">
<div class="card-header" id="heading${i}">
    <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapse${i}" aria-expanded="false" aria-controls="collapse${i}">
           <h2> ${org.name} </h2>
        </button>
    </h2>
</div>
<div id="collapse${i}" class="collapse" aria-labelledby="heading${i}" data-parent="#searchResult">
    <div class="card-body">
    <h3>Organisasjonsnummer:</h3>${org.org_number}
    ${addUrlIfExist(org.url)}
    ${addAddressIfExist(org.tilholdssted_address)}
    ${addPhoneIfExist(org.phone_number)}

    </div>
</div>
</div>`).appendTo('#searchResult')
    })
}
function storforbokstav(text) {
    let lower = text.toLowerCase();
    let first = lower[0].toUpperCase();
    return first + lower.slice(1);
}

function addUrlIfExist(url) {
    if (url) {
        return `<h3>Nettside:</h3><a href="http://${url}">${url}</a>`
    }
    else {
        return ``
    }
}
function addPhoneIfExist(phone_number) {
    if (phone_number) {
        return `<h3>Telefon:</h3><a href="tel:${phone_number}">${phone_number}</a>`
    }
    else {
        return ``
    }
}
function createAddressText(address) {
    if (address) {

        return `${address.address_line}, ${address.postal_code} ${
            address.postal_city
            }`;
    }
    return ``;
};

function addAddressIfExist(address) {
    if (address) {
        let addressText = createAddressText(address)
        return `<h3>Tilholdssted:</h3>
          <a href="https://maps.google.com/?q=${addressText}">${addressText}</a>`
    }
    else
        return ``
}