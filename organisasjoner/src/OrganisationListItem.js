import React from "react";
// import { Link } from "react-router-dom";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

class OrganisationListItem extends React.Component {
  createaddresstext = address => {
    if (address) {
      return `${address.address_line}, ${address.postal_code} ${
        address.postal_city
        }`;
    }
    return ``;
  };

  render() {
    var organisation = this.props.organisation;
    var tilholdssted = this.createaddresstext(
      organisation.tilholdssted_address
    );
    var url = `https://organisasjoner.trondheim.kommune.no${organisation.uri}`;

    return (
      <ExpansionPanel className="OrganisationListItem">
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className="OrganisationListItemInfo">{organisation.name}</Typography>
          <Typography />
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            {organisation.url && (
              <span>
                <b>Nettside: </b>
                <a href={"http://" + organisation.url}>{organisation.url}</a>
                <br />
              </span>
            )}
            {organisation.phone_number && (
              <span>
                <b>Telefon: </b>
                <a href={"tel:" + organisation.phone_number}>
                  {organisation.phone_number}
                </a>
                <br />
              </span>
            )}
            {organisation.tilholdssted_address && (
              <span>
                <b>Adresse: </b>
                {tilholdssted}
                <br />
                <a href={"https://maps.google.com/?q=" + tilholdssted}>
                  {" "}
                  <br />
                  Vis i kart <br />
                </a>
              </span>
            )}
            <a href={url}>Vis registrert informasjon om {organisation.name}</a> <br />
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}
OrganisationListItem.propTypes = {
  organisation: PropTypes.object.isRequired
};

export default OrganisationListItem;
// export default function({ organisation }) {}
