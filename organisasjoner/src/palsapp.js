import React, { Component } from "react";
import "./App.css";

class App extends Component {
  state = {
    organisations: []
  };
  abortController = null;

  handleInput = event => {
    if (event.target.value !== "") {
      this.searchByText(event.target.value);
    }
  };
  searchByText = text => {
    if (this.abortController) {
      this.abortController.abort();
    }
    this.abortController = new AbortController();
    var url =
      "https://organisasjoner-staging.trondheim.kommune.no/api/organisations/v1/organisations/?search_term=";

    return fetch(`${url}${text}`, {
      signal: this.abortController.signal
    })
      .then(response => response.json())
      .then(orgs => this.setState({ organisations: orgs }))
      .catch(err => console.error(err));
  };

  render() {
    return (
      <div>
        <input type="text" onChange={this.handleInput} />
        <ul>
          {this.state.organisations.map(organisation => (
            <li key={organisation.id}>
              <a href={organisation.uri}>{organisation.name}</a>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default App;
