import React from 'react';
import PropTypes from 'prop-types';

class Farger extends React.Component {
render () {
   return (
       <div>
       {this.props.colors.map (color => {
           var stil = {
               backgroundColor: color,
               color: isDarkColor(color)
                   ? "white"
                   : "black",  
           };
           return <p style={stil}></p>Dette er fargen {color}</p>
       }
       </div>
   ) 
}
}

Farger.propTypes = {
 colors = PropTypes.arrayOf(PropTypes.string).isRequired
}

function isDarkColor(color) {
    if (color === "black" || color === "white") {
    return true }
} else
{return false}

export default Farger;
