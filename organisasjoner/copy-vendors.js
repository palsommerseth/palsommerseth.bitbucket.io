var fs = require("fs");
var revHash = require("rev-hash");
var path = require("path");

var files = [
  "node_modules/react/umd/react.production.min.js",
  "node_modules/react-dom/umd/react-dom.production.min.js",
  "node_modules/\@material-ui/core/umd/material-ui.production.min.js",
];

console.log(`--- COPYING VENDOR FILES ---`)

var content = ""
for (var file of files) {
  content += fs.readFileSync(file);
  var filename = path.parse(file);
  console.log(`Copied ${filename.base}`)
}

var hash = revHash(content);
var outputFilename = `vendors.${hash}.js`;
fs.writeFileSync(path.join("public", outputFilename), content);

console.log(`Written to public/${outputFilename}`)
console.log(`-----------------------------`)
console.log(`Include in index.html: <script src="%PUBLIC_URL%/${outputFilename}"></script>`)
console.log(`-----------------------------`)
