import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      postnummer: '1234',
      poststed: '',
    }
  }

  handleInput = (event) => {
    this.setState({
      postnummer: event.target.value})
    var clientUrl = window.location.href
    // var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=asdf&pnr=${event.target.value}`
     var URL = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/${event.target.value}`
    fetch(URL).then(Response => Response.json())
      .then((fraBring) => {
        // oppdater tilstand
        this.setState({
          poststed: fraBring.result})
      })
  }
  render() {
          return(
      <div className = "App" >
              <input defaultValue={this.state.postnummer}  
              onChange={this.handleInput} />
             
              <br />
              <p>{this.state.poststed}</p>
      </div>
    );
  }
}

export default App;
