import React from 'react';
import Typography from '@material-ui/core';
import './App.css';

function App() {
  let [barnehage, setBarnehage] = React.useState({})
  fetch('http://www.barnehagefakta.no/api/Barnehage/1016912')
    .then(r => r.json())
    .then(setBarnehage)

  return (
    <div>
      <div className="tk-barnehage"></div>
      <Typography>{barnehage.fylke}</Typography>
      <pre><code>
        {JSON.stringify(barnehage, null, 2)}
      </code></pre>
    </div>
  );
}

export default App;
