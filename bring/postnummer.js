//Hente postnummer fra Brings API
var postNummerElement = document.querySelector("input#postNummer");
var postStedElement = document.querySelector("input#postSted");

postNummerElement.addEventListener("input", hentPoststed);

function hentPoststed() {
  var postNummer = postNummerElement.value;
  var clientUrl = location.href;
  var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=asdf&pnr=${postNummer}`;
  fetch(URL)
    .then(Response => Response.json())
    .then(function(fraBring) {
      postStedElement.value = fraBring.result;
      console.log(fraBring);
    });
}
