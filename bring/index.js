function pal (x) {
    return x + 1
}

function toparam (x, y) {
    return x + y
}

pal(111)

console.log('tekst som sendes inn')

var petter = 123 //<-- tall
var ida = 'Dette er en tekst-string '
var ola = true //<-- eller false
var maria = {   //<--object
    a: 123,     //<--key: value,
    b: true,
    c: "tekst",
}

document.write(maria.c)  //for å få tak i variabelens nøkkel
var postNummer = 7075
var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=asdf&pnr=${postNummer}`

//DOM

var postNummerElement = document.querySelector('input#postNummer')   //<-- hashtag for id, evt punktum for class: input.red, <input class="red">

//Event

postNummerElement.addEventListener("input", function lytter() {
    console.log(postNummerElement.value)
if (postNummerElement.value.length === 4){
    console.log("Postnummer er fire sifre, og trolig korrekt")
} else {
    console.log("Postnummer er ufullstendig")
}

})