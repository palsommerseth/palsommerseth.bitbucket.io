import React from 'react';
/* import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'; */
import { Header } from "./components/Header";
import { Tekst } from "./components/Tekst";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import CardMedia from "@material-ui/core/CardMedia";
import Button from '@material-ui/core/Button';

function App() {

  const name = 'Arbeidstilsynet'
  return (
    
      <Grid container className='container' spacing={3}>
              <Grid item lg={4} md={4} sm={6} xs={12}>
    <Card>
      <CardContent>
        <CardMedia
        component="img"
        height="140"
        image="https://picsum.photos/seed/picsum/300/170"
        alt="Her kommer en alt-tekst"
      />
     <Typography>
        <Header />
           <h1 className="ws1">{name}</h1>
           {/* <Tekst />
      <p>{tekst}</p> */}
      <Button color="primary" variant="contained">
        Click Me
        </Button>
      </Typography>
      </CardContent>
    </Card>
    </Grid>
    <Grid item lg={4} md={4} sm={6} xs={12}>
    <Card>
      <CardContent>
      <Typography>
     <h1 className='ibm'>1Il 0O ao</h1>
     <Header />
      <h2 className='ibm'>H{1 + 1} {name}</h2>
      <h1 className='ibm'>{name} med IBM Plex</h1>
      <p className='ibm'>Dette er en test på Arbeidstilsynssaker æøå</p>
      <Button color="secondary" className="ibm" variant="contained">
        Click Me
        </Button>
      </Typography>
      </CardContent>
    </Card>
    </Grid>
    </Grid>
         
  );
}

const styles = {
  kort: {
    height: "100%",
  },
};

export default App;
