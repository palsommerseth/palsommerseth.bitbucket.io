export const Header = ({title}) => {
    return (
        <header>
            {title}
        </header>
    )
}
Header.defaultProps = {
    title: 'Tittel fra Header.js',
}
 

export default Header
