// const server = require('./server')
const { launch } = require('puppeteer')
const { equal, notEqual } = require('assert');

// let port = 8888
// let mainPage = `http://localhost:${port}/`
let mainPage = `http://localhost:3000/`
let browser = null
let page = null
let den = it

before(async function () {
    this.timeout(10 * 1000) // starting browser may take more than 2 seconds
    // await server.start(port)
    browser = await launch({
        headless: false,
        // devtools: true,
        // slowMo: 300
    })
    page = (await browser.pages())[0]

    // page.on('console', async function (msg) {
    //     if (msg.type() === 'error' && msg.args().length) {
    //         console.error("Browser console.error:")
    //         let error = await msg.args()[0].jsonValue()
    //         console.error(error)
    //     } else {
    //         console.log(msg._text)
    //     }
    // })
})

beforeEach(async function () {
    this.timeout(5 * 1000)
    await page.goto(mainPage)
})

after(function () {
    browser.close()
    // server.shutdown()
})

den('skal ha et input-element', async function () {
    let input = await page.$('input')
    notEqual(input, null)
})

den('skal finne "Byneset golfklubb" i søket', async function () {
    // this.timeout(30 * 1000)
    let input = await page.$('input')
    await input.type('golf')
    // await input.press('Enter')

    await ventPåSynligElementSomInneholderTekst('BYNESET GOLFKLUBB')
})
den('skal finne organisasjon etter navn', async function () {
    let input = await page.$('input')
    await input.type('berse')
    // await input.press('Enter')

    await ventPåSynligElementSomInneholderTekst('BERSEBLÆSTEN')
})
den('skal finne organisasjon etter kategori', async function () {
    this.timeout(3 * 1000)
    let input = await page.$('input')
    await input.type('orientering')
    // await input.press('Enter')

    await ventPåSynligElementSomInneholderTekst('FREIDIG SPORTSKLUBBEN')
})

den('skal finne organisasjon etter både kategori og navn', async function () {
    this.timeout(3 * 1000)
    let input = await page.$('input')
    await input.type('orientering freidig')
    // await input.press('Enter')

    await ventPåSynligElementSomInneholderTekst('FREIDIG SPORTSKLUBBEN')
})
den('skal vise ekstra informasjon om organisasjon', async function () {
    let input = await page.$('input')
    await input.type('berse')
    // await input.press('Enter')
    await klikkPåSynligElementSomInneholderTekst('BERSEBLÆSTEN')
    await ventPåSynligElementSomInneholderTekst('org.ntnu.no/berse')

    // let elementText = await getTextInElement('.OrganisationListItem p')
    // equal(elementText, 'BERSEBLÆSTEN')
})




function sleep(time) {
    return new Promise(resolve => setTimeout(resolve, time))
}

async function getTextInElement(selector) {
    await page.waitFor(selector)
    return await page.$eval(selector, el => el.textContent)
}
async function ventPåSynligElementSomInneholderTekst(text) {
    await page.waitFor((text) => {
        let matchingElements = Array.from(document.querySelectorAll('*'))
            .filter(element => element.textContent.includes(text))
            .sort((a, b) => a.textContent.length - b.textContent.length) // kortest tekst, altså beste søkeresultat, først

        return matchingElements.length > 0 && matchingElements[0].offsetParent !== null
    }, {}, text)
}

async function klikkPåSynligElementSomInneholderTekst(text) {
    await page.waitForFunction((text) => {
        let matchingElements = Array.from(document.querySelectorAll('*'))
            .filter(element => element.textContent.includes(text))
            .sort((a, b) => a.textContent.length - b.textContent.length) // kortest tekst, altså beste søkeresultat, først

        if (matchingElements.length > 0 && matchingElements[0].offsetParent !== null) {
            matchingElements[0].click()
            return true
        }
        return false
    }, {}, text)
}
