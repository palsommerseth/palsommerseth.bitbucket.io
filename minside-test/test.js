// const server = require('./server')
const { launch } = require('puppeteer')
const { equal, notEqual } = require('assert');

// let port = 8888
// let mainPage = `http://localhost:${port}/`
let mainPage = `https://minsideqa.trondheim.kommune.no`
let browser = null
let page = null
let den = it

before(async function () {
    this.timeout(30 * 1000) // starting browser may take more than 2 seconds
    // await server.start(port)
    browser = await launch({
        headless: false,
        // devtools: true,
        slowMo: 300
    })
    page = (await browser.pages())[0]
    await page.goto(mainPage)
    await waitFor({ text: 'Med koder fra banken din' })
    await waitFor({ text: 'Logg inn med BankID' })
    var frames = await page.frames()
    page = frames[0]

    await waitFor({ text: 'Fødselsnummer', click: true, timeout: 10 * 1000 })
    await page.keyboard.type('11089400300', { delay: 50 })
    await page.waitFor(2 * settle)
    await page.keyboard.type('otp', { delay: 50 })
    await page.keyboard.press('Enter')

    await waitFor({ text: 'Personlig passord', click: true, timeout: 10 * 1000 })
    await page.waitFor(2 * settle)
    await page.keyboard.type('qwer1234', { delay: 50 })
    await page.keyboard.press('Enter')
})

//     await klikkPåSynligElementSomInneholderTekst('Fødselsnummer')


//     // page.on('console', async function (msg) {
//     //     if (msg.type() === 'error' && msg.args().length) {
//     //         console.error("Browser console.error:")
//     //         let error = await msg.args()[0].jsonValue()
//     //         console.error(error)
//     //     } else {
//     //         console.log(msg._text)
//     //     }
//     // })
// })

// beforeEach(async function () {
//     this.timeout(5 * 1000)
//     await page.goto(mainPage)
// })

// after(function () {
//     // browser.close()
//     // server.shutdown()
// })

// den('skal ha et input-element', async function () {
//     let input = await page.$('input')
//     notEqual(input, null)
// })

// async function ventPåSynligElementSomInneholderTekst(text) {
//     await page.waitFor((text) => {
//         let matchingElements = Array.from(document.querySelectorAll('*'))
//             .filter(element => element.textContent.includes(text))
//             .sort((a, b) => a.textContent.length - b.textContent.length) // kortest tekst, altså beste søkeresultat, først

//         return matchingElements.length > 0 && matchingElements[0].offsetParent !== null
//     }, {}, text)
// }

// async function klikkPåSynligElementSomInneholderTekst(text) {
//     await page.waitForFunction((text) => {
//         let matchingElements = Array.from(document.querySelectorAll('*'))
//             .filter(element => element.textContent.includes(text))
//             .sort((a, b) => a.textContent.length - b.textContent.length) // kortest tekst, altså beste søkeresultat, først

//         if (matchingElements.length > 0 && matchingElements[0].offsetParent !== null) {
//             matchingElements[0].click()
//             return true
//         }
//         return false
//     }, {}, text)
// }

async function waitFor({ text = '', selector = '*', click = false, timeout = 10000 }) {
    const start = Date.now()

    while ((Date.now() - start) < timeout) {
        let frames = await page.frames()
        let scopes = [page, ...frames]
        for (let scope of scopes) {
            let result
            try {
                result = await scope.evaluate(pageFunction, text, selector, click)
            } catch (err) {
                // probably lost execution context, break and get new scopes
                break
            }
            if (result) {
                return true
            }
        }
    }

    throw new Error(`'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`)

    function pageFunction(text, selector, click) {
        let match = findElement(text)

        if (match) {
            if (click) {
                match.click()
            }
            return true
        }
        return false

        function findElement(text) {
            let matchingElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.textContent.includes(text))
                .sort((a, b) => a.textContent.length - b.textContent.length) // shortest text first, e.g. "best" search result

            if (matchingElements.length > 0 && matchingElements[0].offsetParent !== null) {
                return matchingElements[0]
            }

            let shadowedElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.shadowRoot)
                .flatMap(element => Array.from(element.shadowRoot.querySelectorAll(selector)))
                .filter(element => element.textContent.includes(text))
                .sort((a, b) => a.textContent.length - b.textContent.length)

            if (shadowedElements.length > 0 && shadowedElements[0].offsetParent !== null) {
                return shadowedElements[0]
            }

            return null
        }
    }
}
async function waitFor({ text = '', selector = '*', click = false, focus = false, timeout = 5000 }) {
    const start = Date.now()

    while ((Date.now() - start) < timeout) {
        let frames = await page.frames()
        let scopes = [page, ...frames]
        for (let scope of scopes) {
            let result
            try {
                result = await scope.evaluate(pageFunction, text, selector, click)
            } catch (err) {
                // probably lost execution context, break and get new scopes
                break
            }
            if (result) {
                return true
            }
        }
    }

    throw new Error(`'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`)

    function pageFunction(text, selector, click) {
        let match = findElement(text, selector)

        if (match) {
            if (click) {
                match.click()
            }
            if (focus) {
                match.focus()
            }
            return true
        }
        return false

        function findElement(text, selector) {
            let matchingElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength) // shortest text first, e.g. "best" search result

            if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
                return matchingElements[0]
            }

            let shadowedElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.shadowRoot)
                .flatMap(element => Array.from(element.shadowRoot.querySelectorAll(selector)))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength)

            if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
                return shadowedElements[0]
            }

            return null
        }

        function shortestTextVisibleIfSameLength(a, b) {
            let difference = a.textContent.length - b.textContent.length;
            if (difference === 0) {
                let aVisible = isVisible(a)
                let bVisible = isVisible(b)

                if (aVisible && !bVisible) {
                    return -1
                } else if (!aVisible && bVisible) {
                    return 1
                } else {
                    return 0
                }
            }
            return difference
        }

        function isVisible(element) {
            return element.offsetParent !== null
        }
    }
}